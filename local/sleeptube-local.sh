#!/bin/bash
#consolidated version of sleeptube to run on a local machine

CLIPBOARD=$(xclip -selection c -o)
echo -e "--------- Clipboard Contents ---------\n$CLIPBOARD"

if [ -z "$CLIPBOARD" ]
	then
		echo "Clipboard NULL, try again"
		exit
fi


	read -n 1 -p "Are Clipboard contents ok? (y/n)" answer
	[ -z "$answer" ] && answer="N"  
	case ${answer:0:1} in
		y|Y|1 )
		    #echo "Yes"
		;;
		* )
		   # echo "No"
			exit
		;;
	esac

	echo -e "\n"

	read -n 1 -p "Quality options availible: 360 (y/1) | 720 (n/2)" answer2
	[ -z "$answer2" ] && answer2="N" 
	echo "" 
	case ${answer2:0:2} in
		y|Y|1 )
			echo "360p video quality selected"
		    QUALITY='18'
		;;
		n|N|2 )
			echo "720p video quality selected"
		    QUALITY='22'
		;;
		* )
		    echo "Other input, 360p selected"
		    QUALITY='18'
		;;
	esac

if [ ! -z "$1" ]
then
	SHUTDOWNTIME="$1"
else
	SHUTDOWNTIME="120"
fi
echo "Shutdown time = $SHUTDOWNTIME"
sudo shutdown -P  +"$SHUTDOWNTIME"

#loop through input and playback
printf %s "$CLIPBOARD" | while read -r line || [ -n "$line" ]
do
	if [ ! -z "$line" ]
	then	
		echo "Current line: $line"
		mpv --geometry=70% --ytdl-format="$QUALITY"/18 --ytdl-raw-options= "$line"
	fi
done


#playback ambient audio

sleep 1 && xset -display :0.0 dpms force off


echo "donezo"
