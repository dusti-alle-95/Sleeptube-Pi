#!/bin/bash
#check clipboard, show vid quality options and prompt to continue

# SSH Credentials
sshline='pi@192.168.1.114'

CLIPBOARD=$(xclip -selection c -o)
echo -e "--------- Clipboard Contents ---------\n$CLIPBOARD"

if [ -z "$CLIPBOARD" ]
	then
		echo "Clipboard NULL, try again"
		exit
fi


	read -n 1 -p "Are Clipboard contents ok? (y/n)" answer
	[ -z "$answer" ] && answer="N"  
	case ${answer:0:1} in
		y|Y|1 )
		    #echo "Yes"
		;;
		* )
		   # echo "No"
			exit
		;;
	esac

	echo -e "\n"

	read -n 1 -p "Quality options availible: 360 (y/1) | 720 (n/2)" answer2
	[ -z "$answer2" ] && answer2="N" 
	echo "" 
	case ${answer2:0:2} in
		y|Y|1 )
			echo "360p video quality selected"
		    QUALITY='18'
		;;
		n|N|2 )
			echo "720p video quality selected"
		    QUALITY='22'
		;;
		* )
		    echo "Other input, 360p selected"
		    QUALITY='18'
		;;
	esac

echo ""
echo "--------- executing ---------"
echo "xclip -selection c -o | ssh "$sshline" \"tee xclip.dump > /dev/null; nohup /home/pi/Sleeptube-Pi/youtubelist-test.sh "$QUALITY" < xclip.dump &\""
echo ""
xclip -selection c -o | ssh "$sshline" "tee xclip.dump > /dev/null; nohup sleeptube.sh $QUALITY < xclip.dump &" 

