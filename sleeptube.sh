#!/bin/bash

#USAGE: xclip -selection c -o | input-to-playback.sh 
exec &> /dev/tty1 #output goes to pi's video out
export TERM=xterm-256color
#variables
VOLUME='1'
ASPECT='stretch'

#clear output
clear 
setterm -cursor off

#check input video quality option 
#18           mp4        480x360    medium , avc1.42001E, mp4a.40.2@ 96k
#22           mp4        1280x720   hd720 , avc1.64001F, mp4a.40.2@192k
if [ ! -z "$1" ]
	then
	vidquality="$1"
		if [ "$vidquality" == "18" ]
			then
			echo "360p video quality selected"
		elif [ "$vidquality" == "22" ]
			then
			echo "720p video quality selected"
		else
			echo "360p video quality selected"
			vidquality='18' 
		fi
	else
			echo "360p video quality selected"
			vidquality='18'
fi

#loop through input and playback
while read -r line || [ -n "$line" ]
do
	if [ ! -z "$line" ]
	then	
		clear
		echo "Current line: $line"
		omxplayer -o both --no-keys --aspect-mode "$ASPECT" --vol "$VOLUME" "$(youtube-dl --verbose -g -c -f "$vidquality"/18 $line)" 
		# --aspect-mode type | Letterbox, fill, stretch	
	fi
done

#turn off screen
/opt/vc/bin/tvservice -o # -p for on

#playback ambient audio
#omxplayer -o both --no-keys --vol "$VOLUME" /home/pi/ambient-audio/dryer.ogg
/home/pi/AmbientZzz-Pi/ambient.sh &

echo "donezo"
