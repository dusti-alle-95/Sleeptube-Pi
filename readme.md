# Sleeptube-Pi by dusti-alle-95

These scripts are designed for one to easily play a list of youtube urls from a text file or the clipboard (linux desktop) on a raspberry pi 3, then turn off the screen and play back an ambient sound (using [AmbientZZz-Pi](https://gitlab.com/dusti-alle-95/AmbientZzz-Pi))

## Installation
* `sudo apt install python-pip omxplayer`
* `sudo pip install youtube-dl`
* Edit wrapper.sh to match your pi's ssh details
* Edit sleeptube.sh to match your preffered ambient sound playback method
* Ensure your pi has enough gpu ram configured using `raspi-config`

## Use Case
1. On your local machine, install wrapper.sh to somewhere in your PATH (e.g. ~/.local/bin) 
2. On your raspberry pi, install sleeptube.sh to /usr/local/bin
3. Use [Copy All Tab Urls WE ](https://addons.mozilla.org/en-US/firefox/addon/copy-all-tab-urls-we/) add-on to copy all open youtube tabs
	* Alternatively, use any other method to copy a list of youtube urls into your clipboard
4. Run wrapper.sh on your local machine
    * Enter your ssh credentials
5. Shutdown your PC, Lay down, relax to some videos and go to sleep!

* Alternatively, any source of text, including the included example list.txt can be piped into sleeptube.sh for playback 
    * `cat list.txt | sleeptube.sh` 
